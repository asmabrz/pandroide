This repository contains the code and the report of our project "Representation and relative positioning from visual information".

The principal goal is to make a Thymio associated with a Raspberry-Pi with only a camera to rebuild very roughly its environment within a 3D java engine (JME3) and to self-localize.

For that, we have developed an exploration strategy which allow the robot doing so.