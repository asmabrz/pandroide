\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {1.1}Contexte}{2}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{}{2}
\contentsline {section}{\numberline {1.2}Objectifs}{2}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{3}
\contentsline {section}{\numberline {1.3}Pr\IeC {\'e}sentation de l'\IeC {\'e}quipe}{3}
\contentsline {subsection}{\numberline {1.3.1}Ma\IeC {\^\i }trise d'\IeC {\oe }uvre}{3}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{3}
\contentsline {subsection}{\numberline {1.3.2}Ma\IeC {\^\i }trise d'ouvrage}{3}
\contentsline {paragraph}{}{3}
\contentsline {chapter}{\numberline {2}Analyse du projet}{4}
\contentsline {paragraph}{}{4}
\contentsline {section}{\numberline {2.1}\IeC {\'E}tat de l'art}{4}
\contentsline {paragraph}{}{4}
\contentsline {section}{\numberline {2.2}\IeC {\'E}tat actuel de l'application}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {chapter}{\numberline {3}Description des besoins}{5}
\contentsline {section}{\numberline {3.1}Architecture du syst\IeC {\`e}me \IeC {\`a} d\IeC {\'e}velopper}{5}
\contentsline {paragraph}{}{5}
\contentsline {paragraph}{}{5}
\contentsline {section}{\numberline {3.2}Acteurs}{6}
\contentsline {section}{\numberline {3.3}Analyse des besoins fonctionnels}{6}
\contentsline {section}{\numberline {3.4}Analyse des besoins non fonctionnels}{6}
\contentsline {subsection}{\numberline {3.4.1}Crit\IeC {\`e}res d'acceptabilit\IeC {\'e}}{6}
\contentsline {paragraph}{}{6}
\contentsline {paragraph}{}{6}
\contentsline {paragraph}{}{6}
\contentsline {subsection}{\numberline {3.4.2}Contraintes}{6}
\contentsline {subsubsection}{Contraintes de d\IeC {\'e}lai}{6}
\contentsline {paragraph}{}{6}
\contentsline {paragraph}{}{7}
\contentsline {subsubsection}{Contraintes mat\IeC {\'e}rielles}{7}
\contentsline {paragraph}{}{7}
\contentsline {paragraph}{}{7}
\contentsline {paragraph}{}{7}
\contentsline {section}{\numberline {3.5}Sp\IeC {\'e}cifications des caract\IeC {\'e}ristiques fonctionnelles}{7}
\contentsline {subsection}{\numberline {3.5.1}Diagramme de cas d'utilisation}{7}
\contentsline {paragraph}{}{8}
\contentsline {chapter}{\numberline {4}Livraison du produit}{9}
\contentsline {paragraph}{}{9}
\contentsline {paragraph}{}{9}
\contentsline {chapter}{\numberline {5}Conclusion}{10}
