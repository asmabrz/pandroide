\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {french}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Contexte}{3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Pr\IeC {\'e}sentation de l'\IeC {\'e}quipe}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Ma\IeC {\^\i }trise d'\IeC {\oe }uvre}{3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}Ma\IeC {\^\i }trise d'ouvrage}{4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Organisation du temps de travail}{4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}\IeC {\'E}tat de l'art}{6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.0.1}Am\IeC {\'e}lioration de travaux existants}{6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.0.2}Br\IeC {\`e}ve revue de la litt\IeC {\'e}rature}{7}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{7}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Analyse des besoins et conception}{8}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Analyse des besoins}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Acteurs}{8}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Besoins fonctionnels}{8}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{8}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Analyse des besoins non fonctionnels}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Crit\IeC {\`e}res d'acceptabilit\IeC {\'e}}{8}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Contraintes}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Contraintes de d\IeC {\'e}lai}{9}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{9}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Contraintes mat\IeC {\'e}rielles}{9}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{9}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Sp\IeC {\'e}cifications des caract\IeC {\'e}ristiques fonctionnelles}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Diagramme de cas d'utilisation}{9}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Diagramme de s\IeC {\'e}quences}{10}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{10}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Pr\IeC {\'e}sentation du mat\IeC {\'e}riel utilis\IeC {\'e}}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Mat\IeC {\'e}riel physique}{12}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{12}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{12}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Le raspberry Pi}{13}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Le robot Thymio}{13}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}La cam\IeC {\'e}ra Pi}{13}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Mat\IeC {\'e}riel logiciel}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}OpenCV}{13}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}JMonkey Engine}{13}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Travail r\IeC {\'e}alis\IeC {\'e}}{14}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Am\IeC {\'e}lioration des travaux pr\IeC {\'e}c\IeC {\'e}dents}{14}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Traitement d'image}{14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{14}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Optimisation des communications entre le robot autonome et l'ordianteur}{14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}\IeC {\'E}volution du programme}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Contr\IeC {\^o}le du robot autonome \IeC {\`a} distance}{15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}M\IeC {\'e}thode d'exploration d\IeC {\'e}velopp\IeC {\'e}e}{15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{16}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{16}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Exp\IeC {\'e}rimentations}{16}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.1}Pr\IeC {\'e}sentation de l'application}{16}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{16}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.2}\IeC {\'E}tude des exp\IeC {\'e}riences effectu\IeC {\'e}es}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Premi\IeC {\`e}re approche na\IeC {\"\i }ve}{17}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{17}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{17}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{17}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{17}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{18}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Exploration enti\IeC {\`e}re de l'environnement}{18}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{18}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Mur non captur\IeC {\'e} en un coup}{19}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Conclusion}{20}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Travail r\IeC {\'e}alis\IeC {\'e}}{21}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Am\IeC {\'e}lioration des travaux pr\IeC {\'e}c\IeC {\'e}dents}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.1.1}M\IeC {\'e}thode d'exploration d\IeC {\'e}velopp\IeC {\'e}e}{21}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Cas A: En face d'un coin}{21}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{21}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{21}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Cas B: Translation}{22}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{22}
